## Ansible AMI builder playbook

It's a simple Ansible playbook that you can use to provision an AMI from

running instances.

The latest playbook release support the integration with the Team communication
tool Slack. Below the set of variables and the usage purpose.

Is possible configure three different Slack integration:

- Using the Ansible callback plugin slack_plugin included in the plugins folder
- Enabling the tasks just switching the slack_enabled default variable to true
- Using both.

In both cases your MUST be configure the Slack specific configuration properties
using the variable below.

## Variables

The variables are stored in the vars/defaults.yml file.
They have the defaults below. The only variable that you must change are the AWS specific.

```
## Slack specific configuration attributes
slack_channel: "#pippo"                         # Slack channel name. *Required if slack_enabled is true*
slack_username: "Ansible AWS AMI Builder"       # Slack username for Ansible. *Required only if slack_enabled is true*
slack_enabled: false                            # On/Off switch for the Slack notification feature. *Optional*

## AWS specific configuration attributes
aws_region: "eu-west-1"                         # AWS Region to use. *Required*
aws_ami_build_number: 01                        # Alphanum build identifier. *Required*
application_name: "basic-os"                    # Application backed in the AMI. *Required*
source_instance_name: "instance-name"           # Source instance name. *Required*
source_instance_status: "running"               # Current state of the instance used to build the AMI. *Required*
aws_ami_state: present                          # AWS AMI state. present => registered, absent=> derigestered. *Required*
```

The information to use for access to AWS and Slack account are stored in the vars/credentials.yml file.

```
### AWS Access credentials
aws_access_key: "################"                           # AWS Access Key. *Required*
aws_secret_key: "#########################################"  # AWS Secret Key. *Required*

### Slack Webhook token
slack_token: "#############################################" # Slack token. *Required only if use slack*
```

## Usage

After set the proper variable if you want use the callback plugin to send the playbook recaps
to Slack edit the ansible.cfg file in the current folder and uncomment the lines below:

```
#callback_plugins = plugins
#callback_whitelist = slack_callback
```

```bash
ansible-playbook playbook.yml
```

License
-------

BSD

Author Information
------------------

Mauro Medda
